import Project from "./Project";
const projectData2d = import.meta.globEager("./projects/2d/**/projectDefinition.ts",) 
const projectData3d = import.meta.globEager("./projects/3d/**/projectDefinition.ts",) 

const store: Record<string, Project> = {}


const projects2d: Project[] = Object.values(projectData2d).map(e => e.default) as any
const projects3d: Project[] = Object.values(projectData3d).map(e => e.default) as any


for(const project of [...projects2d, ...projects3d]) {
    store[project.id] = project;
}


export function hasRecord(value: string) {
    return !!getRecord(value);
}

export function getRecord(value: string) : Project | undefined {
    return store[value];
} 

export {projects2d, projects3d}