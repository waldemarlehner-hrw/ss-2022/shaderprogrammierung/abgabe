#ifdef GL_ES
precision mediump float;
#endif
#pragma glslify: Camera = require("../lib/camera.glsl")
#pragma glslify: Ray = require("../lib/ray.glsl")
#pragma glslify: LIB_noise = require("../../../lib/noise.glsl);
#pragma glslify: LIB_random = require("../../../lib/random.glsl);

#pragma glslify: LIB_makeRayFromCamera = require("../lib/camera_ray.glsl");
#pragma glslify: getRandom = require("../../../lib/random.glsl);
#pragma glslify: hsv2rgb = require("glsl-hsv2rgb");

const int METASPHERE_COUNT = 3;

struct Metasphere {
    vec3 position;
    float size;
    float reflectivity; // 1 -> Mirror-Sphere, 0 -> Completely Refractive
};


float getSizeForIndex(int i, float time) {
    float randOffset = getRandom(float(i));
    float randMultiplier = getRandom(randOffset);

    return 1. + ((cos(randOffset + time) * .5) + .5) * 2.;
}

vec3 getPositionForIndex(int i, float time) {
    float start = float(i)/float(METASPHERE_COUNT) * 6.28318530718; // * 2 pi
    start += time * .1;

    float x = sin(start);
    float z = cos(start);
    float y = sin(start);

    return vec3(x,y,z) * 3.;
}

float getReflectivityForIndex(int i) {
    const float STATIC_SEED = 0.0765;
    float inputVal = float(i) + STATIC_SEED;
    return getRandom(inputVal);
}

/**
 * Returns the Normal Vector at a given position. Approximation looking at size/distance to definitions
 */
vec3 approximateMetaSphereNormal(Metasphere metaspheres[METASPHERE_COUNT], vec3 position) {
    vec3 normal = vec3(0.);
    for(int i = 0; i < METASPHERE_COUNT; i++) {
        Metasphere current = metaspheres[i];
        float dist = distance(current.position, position);
        float weight = current.size / (dist*dist);
        vec3 dir = normalize(position - current.position);
        normal += dir * weight;
    }

    return normalize(normal);
}


/**
 * Sets the new Values for Position and Size of Metaspheres. Dependent on Time
 */
void manipulateMetaSphereState(float time, out Metasphere metaspheres[METASPHERE_COUNT]) {
    for(int i = 0; i < METASPHERE_COUNT; i++) {
        metaspheres[i].size = getSizeForIndex(i, time);
        metaspheres[i].position = getPositionForIndex(i, time);
        metaspheres[i].reflectivity = getReflectivityForIndex
    (i);
    }
}

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

uniform sampler2D u_skybox;

const float PI = 3.14159;


float evaluateStateAtPosition(Metasphere state[METASPHERE_COUNT], vec3 position) {
    float val = 0.;
    for (int i = 0; i < METASPHERE_COUNT; i++) {
        Metasphere current = state[i];
        float dist = distance(position, current.position);
        val += current.size / dist;
    }

    return val;
}


float calculateReflectivity(Metasphere state[METASPHERE_COUNT], vec3 collision, float sum) {
    // Get the Reflectivity here
    float reflectivity = 1.;
    for(int i=0; i < METASPHERE_COUNT; i++) {
        Metasphere current = state[i];

        if(current.size > .0) {
            float dist = distance(collision, current.position);
            float value = current.size / dist;    
            reflectivity = mix(reflectivity, current.reflectivity, value/sum);
        }
    }
    return reflectivity;
}


/**
 * Returns the reflectivity [0,1] and r 
 */
float calculateRayCollision(Ray r, Metasphere state[METASPHERE_COUNT], float evalCutOff, bool isInside) {
    const int LINEAR_ITERATIONS = 100;
    const int BINARY_ITERATIONS = 10;
    float furthestSphereDistanceSqrt = -1e20;
    for (int i = 0; i < METASPHERE_COUNT; i++) {
        vec3 currentPos = state[i].position;
        vec3 dPos = currentPos - r.pos;
        float distSqrt = dPos.x*dPos.x + dPos.y*dPos.y + dPos.z*dPos.z;
        // Add magnitude to distSqrt
        distSqrt += state[i].size * 10.;
        if (distSqrt > furthestSphereDistanceSqrt) {
            furthestSphereDistanceSqrt = distSqrt;
        }
    }
    float collisionUpper = sqrt(furthestSphereDistanceSqrt);
    float collisionLower = 0.;
    bool hasCollision = false;
    
    // Do Linear Iterations
    for (int i = 0; i < LINEAR_ITERATIONS; i++) {
        float currentFraction = float(i)/float(LINEAR_ITERATIONS);
        vec3 pos = r.pos + r.dir * (currentFraction * collisionUpper);
        float result = evaluateStateAtPosition(state, pos);
        bool successfulCutoff = isInside ? result < evalCutOff : result > evalCutOff; 
        if (successfulCutoff) {
            collisionLower = float(i-1)/float(LINEAR_ITERATIONS) * collisionUpper;
            collisionUpper *= currentFraction;
     
            hasCollision = true;
            break;
        }
    }

    if (!hasCollision) {
        return -10.;
    }

    // Do Binary Iterations
    float currentR;
    for(int i = 0; i < BINARY_ITERATIONS; i++) {
        currentR = (collisionLower + collisionUpper) * .5;
        vec3 pos = r.pos + r.dir * currentR;
        float result = evaluateStateAtPosition(state, pos);

        bool successfulCutoff = isInside ? result < evalCutOff : result > evalCutOff; 

        if(successfulCutoff) {
            collisionUpper = currentR;
        } else {
            collisionLower = currentR;
        }
    }

    return currentR;

}

float calculateRayCollision(Ray r, Metasphere state[METASPHERE_COUNT], float evalCutOff) {
    return calculateRayCollision(r, state, evalCutOff, false);
}


float noiseStacked(vec2 pos) {
    // Stacking Noises over each other. 
    // Each new Layer has finer detail and a lower amplitude. (see FBM)
    const int ITERATION_COUNT = 6;
    float value = 0.;
    for (int i = 0; i < ITERATION_COUNT; i++) {
        vec2 probe = pos * float(i+1) * 4. + LIB_random(float(i)) * 20.;
        float val = LIB_noise(probe);
        value += (val / float(i+3));
    }
    // Multiply with itself. No other reason that I liked the look of it more :)
    value *= value;
    return value;
}


vec3 getSkyboxColor(vec3 dir) {
    float u = atan(dir.x, dir.z) / (2.*PI);
    u = fract(u);
    // map u (0,1) to (0 -> 1 -> 0)
    u = (2.*u) - 1.;
    float v = dir.y * .5 + .5;
    vec2 uv = vec2(u,v);
    vec2 st = uv * 20.;

    // This is a modification of my "Noise Parallax Stack" 2D-Project.
    // For extensive documentation, please head there.
    float result = noiseStacked(st);

    const vec3 backgroundColor = vec3(.05);
    const vec3 ring1Color = vec3(1., .0, .0);
    const vec3 ring2Color = vec3(1., 1.0,.0);
    const vec3 ring3Color = vec3(1., .0, 1.);

    if (result < .2) return backgroundColor;
    if (result < .25) return ring1Color;     
    if (result < .4) return backgroundColor;
    if (result < .45) return ring2Color;     
    if (result < .60) return backgroundColor;
    if (result < .65) return ring3Color;
    return backgroundColor; 

}


// value MUST be > 1
const float REFRACTION_VALUE = 3.;



void main() {
    vec2 st = gl_FragCoord.xy / u_resolution;
    
    float time_remap = u_time * 0.2;
    const float camDist = 20.;
    Camera cam = Camera(
        vec3(sin(time_remap) * camDist, sin(time_remap) * camDist, cos(time_remap) * camDist),
        vec3(0), 
        60., 
        u_resolution.x/u_resolution.y
    );

    Ray ray = LIB_makeRayFromCamera(cam, st);

    Metasphere state[METASPHERE_COUNT];
    manipulateMetaSphereState(time_remap, state);

    float resultR = calculateRayCollision(ray, state, 1.5);
    vec3 outColor = vec3(0.);
    if (resultR > -.5) {
    // There is a Collision between Camera and a Sphere
        outColor = vec3(0.);
        // get the Collision Point
        vec3 firstCollisionPoint = ray.pos + ray.dir * resultR;
        vec3 normalOfFirstCollisionPoint = approximateMetaSphereNormal(state, firstCollisionPoint);
        // This is the direction "inside" the sphere after refraction.

        /// !!!
        ///      I had to set the negative normal to get good results
        /// !!!                                 v note the minus
        vec3 firstRefraction = refract(ray.dir, -normalOfFirstCollisionPoint, 1./REFRACTION_VALUE);
        // step a bit "inside" to avoid immediate collision, then look for where the 
        Ray insideRay = Ray(firstCollisionPoint + -normalOfFirstCollisionPoint * .01, firstRefraction);
        // Find where the ray collides "on the inside" with the wall
        float insideResult = calculateRayCollision(insideRay, state, 1.5, true);
        if (insideResult < -.5) {
            // No Collision inside. Should not happen. But if for some reason it does, just return 
            // the sample from the skybox.
            outColor = getSkyboxColor(firstRefraction);
            outColor = vec3(.5);
        } else {
            vec3 secondCollisionPoint = insideRay.pos + insideRay.dir * insideResult;
            vec3 normalOfSecondCollisionPoint = approximateMetaSphereNormal(state, secondCollisionPoint);
            // Now refract "backwards".
            // This is the direction after refracting back.
            vec3 secondRefraction = refract(insideRay.dir, normalOfSecondCollisionPoint, REFRACTION_VALUE);
            // check if the result is "correct"
            if(length(secondRefraction) == 0.) {
                outColor = vec3(1., 0., 0.);
                outColor = getSkyboxColor(firstRefraction);

            } else {
                // There could be an outbound ray.. but we only really care about the direction anyways.. so no
                // need to construct an entire ray here.
                vec3 outboundDirection = secondRefraction;
                outColor = getSkyboxColor(outboundDirection);
            }
        }
    } else {
        // No Collision. Show Skybox
        outColor = getSkyboxColor(ray.dir);
    }
    


    gl_FragColor = vec4(outColor, 1.);
}