#ifdef GL_ES
precision mediump float;
#endif
#pragma glslify: Camera = require("../lib/camera.glsl")
#pragma glslify: Ray = require("../lib/ray.glsl")

#pragma glslify: LIB_makeRayFromCamera = require("../lib/camera_ray.glsl");
#pragma glslify: getRandom = require("../../../lib/random.glsl);

const int METASPHERE_COUNT = 3;

struct Metasphere {
    vec3 position;
    vec3 color;
    float size;
    float reflectivity;
};

// import a util that converts hsv to normalized rgb
#pragma glslify: hsv2rgb = require("glsl-hsv2rgb");

// Get a colour for the Metasphere with the provided index i
vec3 getColorForIndex(int i, float time) {
    const float STATIC_SEED = 0.3252;
    float inputVal = float(i) + STATIC_SEED;

    float hue = getRandom(inputVal);
    return hsv2rgb(vec3(hue, 1., 1.));
}

// Get the size for the Metasphere with the provided index i
float getSizeForIndex(int i, float time) {
    float randOffset = getRandom(float(i));
    float randMultiplier = getRandom(randOffset);

    return 1. + ((cos(randOffset + time) * .5) + .5) * 2.;
}

// same for a Position
vec3 getPositionForIndex(int i, float time) {
    float start = float(i)/float(METASPHERE_COUNT) * 6.28318530718; // * 2 pi
    start += time * .1;

    float x = sin(start);
    float z = cos(start);
    float y = sin(start);

    return vec3(x,y,z) * 3.;
}

// and the reflectivity
float getReflectivityForIndex(int i) {
    const float STATIC_SEED = 0.0765;
    float inputVal = float(i) + STATIC_SEED;
    return getRandom(inputVal);
}

/**
 * Returns the Normal Vector at a given position. Approximation looking at size/distance to definitions.
   It is NOT 100% correct, but it is good enough!
 */
vec3 approximateMetaSphereNormal(Metasphere metaspheres[METASPHERE_COUNT], vec3 position) {
    vec3 normal = vec3(0.);
    for(int i = 0; i < METASPHERE_COUNT; i++) {
        Metasphere current = metaspheres[i];
        float dist = distance(current.position, position);
        float weight = current.size / (dist*dist);
        vec3 dir = normalize(position - current.position);
        normal += dir * weight;
    }

    return normalize(normal);
}


/**
 * Sets the new Values for Position and Size of Metaspheres. Dependent on Time
 */
void manipulateMetaSphereState(float time, out Metasphere metaspheres[METASPHERE_COUNT]) {
    for(int i = 0; i < METASPHERE_COUNT; i++) {
        metaspheres[i].color = getColorForIndex(i, time);
        metaspheres[i].size = getSizeForIndex(i, time);
        metaspheres[i].position = getPositionForIndex(i, time);
        metaspheres[i].reflectivity = getReflectivityForIndex(i);
    }
}

uniform vec2 u_resolution;
uniform float u_time;

uniform sampler2D u_skybox;

const float PI = 3.14159;


float evaluateStateAtPosition(Metasphere state[METASPHERE_COUNT], vec3 position) {
    float val = 0.;
    for (int i = 0; i < METASPHERE_COUNT; i++) {
        Metasphere current = state[i];
        float dist = distance(position, current.position);
        val += current.size / dist;
    }

    return val;
}
/**
 *  This will find the collision in the Metasphere-Field.
 *  It uses a mix of linear and binary probing to find the point of collision.
 *
 *
 * @returns the color (r,g,b), and r 
 */
vec4 calculateRayCollision(Ray r, Metasphere state[METASPHERE_COUNT], float evalCutOff) {
    const int LINEAR_ITERATIONS = 100;
    const int BINARY_ITERATIONS = 10;
    float furthestSphereDistanceSqrt = -1e20;
    
    // first create approximated bounds to get the most out of linear iterations.
    // there isnt any proper math in here. Just get the distance squared and add the size * 10 as a
    // small buffer. This should get a more or less good aproximation that overestimates the distance.
    for (int i = 0; i < METASPHERE_COUNT; i++) {
        vec3 currentPos = state[i].position;
        vec3 dPos = currentPos - r.pos;
        float distSqrt = dPos.x*dPos.x + dPos.y*dPos.y + dPos.z*dPos.z;
        // Add magnitude to distSqrt
        distSqrt += state[i].size * 10.;
        if (distSqrt > furthestSphereDistanceSqrt) {
            furthestSphereDistanceSqrt = distSqrt;
        }
    }
    // Set the upper and lower bounds for linear probing
    float collisionUpper = sqrt(furthestSphereDistanceSqrt);
    float collisionLower = 0.;
    bool hasCollision = false;
    
    // start with the linear probing
    // Eval the Metaspheres until the result is > evalCutoff.
    // The r to probe is mapped onto [collisionLower, collisionUpper]
    for (int i = 0; i < LINEAR_ITERATIONS; i++) {
        float currentFraction = float(i)/float(LINEAR_ITERATIONS);
        vec3 pos = r.pos + r.dir * (currentFraction * collisionUpper);
        float result = evaluateStateAtPosition(state, pos);
        if (result > evalCutOff) {
            // The result was bigger than the cutoff. This means that the "edge" is between
            // this and the last evaluation.
            // collisionLower and Upper are being updated to reflect that.
            collisionLower = float(i-1)/float(LINEAR_ITERATIONS) * collisionUpper;
            collisionUpper *= currentFraction;
     
            hasCollision = true;
            break;
        }
    }

    // If Linear Probing didnt get a result, just return early as there 
    // are not bounds that can be used to do binary search.
    if (!hasCollision) {
        return vec4(vec3(0.), -10.);
    }

    // Do Binary probing
    float currentR;
    float sum; // used for color
    for(int i = 0; i < BINARY_ITERATIONS; i++) {
        // Probe inbetween the upper and lower bound.
        currentR = (collisionLower + collisionUpper) * .5;
        vec3 pos = r.pos + r.dir * currentR;
        float result = evaluateStateAtPosition(state, pos);

        // if the result is bigger than the cutoff, the edge must be in the lower half
        if(result > evalCutOff) {
            collisionUpper = currentR;
        } 
        // else if the result is smaller than the cutoff, the edge must be in the upper half
        else {
            collisionLower = currentR;
        }

        // also set the sum from the result. This way after all binary iterations are done this
        // value can be used to set the correct color more efficiently, as one summation of all
        // spheres is skipped.
        sum = result;
    }
    // Get the Color here
    vec3 color = vec3(1.);
    for(int i=0; i < METASPHERE_COUNT; i++) {
        Metasphere current = state[i];
        vec3 position = r.pos + currentR * r.dir;
        if(current.size > .0) {
            float dist = distance(position, current.position);
            float value = current.size / dist;
            // Mix the color for each sphere based on its evaluation value weighted towards the total sum.
            color = mix(color, current.color, value/sum);
        }
    }

    // at the end, return the color and the r.
    return vec4(color, currentR);

}

// A simple Skybox Sample. Convert dir into uv-Coordinates.
vec3 getSkyboxColor(vec3 dir) {
    float u = atan(dir.x, dir.z) / (2.*PI);
    u = fract(u);
    float v = dir.y * .5 + .5;
    vec2 uv = vec2(u,v);
    
    vec4 tex = texture2D(u_skybox, uv);
    return tex.xyz;
}

// Each Metasphere has a reflectivity value. This function interpolates between the
// metaspheres to get the reflectivity of the Metasphere Field at the given Position
float calculateReflectivity(Metasphere state[METASPHERE_COUNT], vec3 position) {
    float reflectSum = 0.;
    float evalSum = 0.;

    // For each Metasphere, "evaluate" the field and add the weighted reflectivity to 
    // reflectSum and total evaluation to evalSum 
    for(int i = 0; i < METASPHERE_COUNT; i++) {
        Metasphere current = state[i];
        float dist = distance(position, current.position);
        float eval = current.size / (dist*dist);
        reflectSum += eval * current.reflectivity;
        evalSum += eval;
    }

    // Now you have a fraction defining how reflective the field is at this position.
    return reflectSum / evalSum;
}


void main() {
    // Correct st
    vec2 st = gl_FragCoord.xy / u_resolution;
    
    float time_remap = u_time * 0.2;
    // Setup Camera
    const float camDist = 20.;
    Camera cam = Camera(
        vec3(sin(time_remap) * camDist, sin(time_remap) * camDist, cos(time_remap) * camDist),
        vec3(0), 
        60., 
        u_resolution.x/u_resolution.y
    );

    // Set up the "state" of the Metaspheres, which contains all Metaspheres with their current
    // position, size, colour and reflectivity.
    Metasphere state[METASPHERE_COUNT];
    manipulateMetaSphereState(time_remap, state);
  
    /// !!! See https://imgur.com/a/3dbGqr5 !!!
    
    // Generate Ray based on St and Camera
    // "ray" here is [A] in the Image linked above
    Ray ray = LIB_makeRayFromCamera(cam, st);
    vec3 lightPosition = vec3(-5., -10., 5.);

    // Get the result for the first collision
    vec4 result = calculateRayCollision(ray, state, 1.5);
    float rResult = result.a;
    vec3 outColor = result.rgb;
    if (rResult > -.5) {
        /// We have a collision (red circle in image)
        vec3 collision = ray.pos + ray.dir * rResult;
        // Get the Normal
        vec3 normal = approximateMetaSphereNormal(state, collision);
        // Dot product between Light and Normal
        float lightDot = dot(normal, normalize(collision-lightPosition));
        outColor *= (lightDot * .5) + .5;

        // Based on Reflectivity of Material Color, add reflection
        float reflectivity = calculateReflectivity(state, collision);
        vec3 reflectionColor;
        {
            // Reflection from first coliision is calculated here
            // This is [B] in the Picture
            Ray reflection = Ray(collision + normal * 0.1, reflect(ray.dir, normal));
            // Collision from the 1st reflection (yellow in Image)
            vec4 reflectionResult = calculateRayCollision(reflection, state, 1.5);

            reflectionColor = reflectionResult.rgb;
            if(reflectionResult.a < -0.0) {
                // No Collision. Sample the Skybox instead
                reflectionColor = getSkyboxColor(reflection.dir);
            } else {
                // Look at reflection in Reflection
                // Collision here is yellow circle in Image
                vec3 collision2 = reflection.pos + reflection.dir * reflectionResult.a;
                float reflectivity2 = calculateReflectivity(state, collision2);
                vec3 normal2 = approximateMetaSphereNormal(state, collision2);
                // Create a reflection from the reflection. The ray here is pink in the Image
                Ray reflection2 = Ray(collision2 + normal2 * .1, reflect(reflection.dir, normal2));
                vec4 reflectionResult2 = calculateRayCollision(reflection2, state, 1.5);

                if (reflectionResult2.a < .5) {
                    // No Collision. Sample the Skyobx instead
                    reflectionColor = mix(reflectionColor, getSkyboxColor(reflection2.dir), reflectivity2);
                } else {
                    reflectionColor = mix(reflectionColor, reflectionResult2.rgb, reflectivity2);
                }
            }
        }
        outColor = mix(outColor, reflectionColor, reflectivity);


    } else {
        // No Collision. Show Skybox
        outColor = getSkyboxColor(ray.dir);
    }
    


    gl_FragColor = vec4(outColor, 1.);
}