import Project from "../../../Project";

import frag from "./main.glsl";
import skybox from "../lookAt/res/skybox.jpg"

export default new Project(
    "Metasphere Raymarching", 
    "Implementierung von reflektiven Metaspheres mithilfe von Raymarching (ohne Signed Distance Functions)", 
    "metasphereRM", 
    frag,
    "https://gitlab.com/waldemarlehner-hrw/ss-2022/shaderprogrammierung/abgabe/-/blob/master/src/data/projects/3d/metasphere-raymarching/main.glsl",
    {
        u_skybox: skybox
    }
);

