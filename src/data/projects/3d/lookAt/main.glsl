#ifdef GL_ES
precision mediump float;
#endif
#pragma glslify: Camera = require("../lib/camera.glsl")
#pragma glslify: Ray = require("../lib/ray.glsl")
#pragma glslify: LIB_makeRayFromCamera = require("../lib/camera_ray.glsl");
#pragma glslify: getRandom = require("../../../lib/random.glsl);




uniform vec2 u_resolution;
uniform float u_time;

uniform sampler2D u_skybox;

const float PI = 3.14159;

// A simple Skybox Sample. Convert dir into uv-Coordinates.
vec3 getSkyboxColor(vec3 dir) {
    float u = atan(dir.x, dir.z) / (2.*PI);
    u = fract(u);
    float v = dir.y * .5 + .5;
    // The UV-Coords are used to sample the Skybox Texture
    vec2 uv = vec2(u,v);
    
    vec4 tex = texture2D(u_skybox, uv);
    // return the Color from the Texture sampling.
    return tex.xyz;
}



const float TEXEL_MULTIPLIER = 10.;
ivec3 getLocalTexelIndex(vec3 position) {
    vec3 localPos = position / TEXEL_MULTIPLIER;
    ivec3 index = ivec3(floor(localPos -= .5 * TEXEL_MULTIPLIER));
    return index;
}

vec3 getLocalPosition(vec3 position) {
    vec3 localPos = position / TEXEL_MULTIPLIER;
    localPos = fract(localPos);
    localPos -= vec3(.5);
    localPos *= TEXEL_MULTIPLIER;
    return localPos;
}

// Evaluates the Signed Distance Field. The SDF here is very basic - just a sphere.
float evaluateDistanceField(vec3 position) {
    // Convert to "Local" Position, basically to the position inside the local "texel
    position = getLocalPosition(position);
    // Sphere at centre
    float len = length(position);
    const float size = 1.;
    if (len < size) {
        return 0.;
    }
    // Return distance towards the edge towards the nearest sphere.
    return len - size;
}


void main() {
    // Correct st
    vec2 st = gl_FragCoord.xy / u_resolution;
    
    float time_remap = u_time * 0.2;
    // Set up camera position
    const float camDist = 20.;
    Camera cam = Camera(
        vec3(sin(time_remap) * camDist, sin(time_remap) * camDist, cos(time_remap) * camDist),
        vec3(0), 
        60., 
        u_resolution.x/u_resolution.y
    );
    // Create Ray for this pixel 
    Ray ray = LIB_makeRayFromCamera(cam, st);
    // Define a Point Light
    vec3 lightPosition = vec3(5., 10., 3.);

    // How many iterations for the signed distance field?
    const int DIST_ITERATIONS = 200;
    // At which distance should a hit be considered?
    const float DIST_CUTTOFF = .01;

    // Now iterate up to 200 times and try to find a collision.
    // Each Iteration the distance field is evaluated. The returns
    // the distance to the closest point of something collidable.
    // This way, in the next iteration a position further down the ray can be looked at
    float currentMultiplier = 0.;
    bool didHit = false;
    for ( int i = 0; i < DIST_ITERATIONS; i++) {
        // Get the current position based on our ray starting position and the current multiplier.
        vec3 currentPos = ray.pos + currentMultiplier * ray.dir;
        float distanceLowerBound = evaluateDistanceField(currentPos);
        currentMultiplier += distanceLowerBound;
        if (distanceLowerBound < DIST_CUTTOFF) {
            // if the distance returned is lower than the cutoff a hit is assumed. In this case, break out of the loop prematurely.
            didHit = true;
            break;
        }
    }

    vec3 color;
    if(!didHit) {
        // if there is no hit, use Skybox Color 
        color = getSkyboxColor(ray.dir);
    } else {
        // else get the point of collision
        vec3 hit = ray.pos + currentMultiplier * ray.dir;
        // get its normal, which is just the collision in local space, normalized
        vec3 normal = normalize(getLocalPosition(hit));
        // get a vector pointing in the direction of the light from the collision.
        vec3 hitToLight = normalize(lightPosition-hit);
        // using dot we get a light intensity. a "direct" hit return 1, 90° hit return 0., etc
        float lightIntensity = dot(normal, hitToLight);
        // Clamp the light intensity to .05
        if(lightIntensity < .05) {
            lightIntensity = .05;
        }
        // The color is the normal value multiplied with the intensity.
        color = lightIntensity * normal;
    }




    gl_FragColor = vec4(color, 1.);
}