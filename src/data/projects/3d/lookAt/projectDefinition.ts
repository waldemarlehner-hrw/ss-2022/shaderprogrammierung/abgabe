import Project from "../../../Project";

import frag from "./main.glsl";
import skybox from "./res/skybox.jpg?url"

export default new Project(
    "Camera Lookat", 
    "Eine Demo der LookAt-Funktion auf einer Szene mit einer Signed Distance Function.", 
    "camLookat", 
    frag,
    "https://gitlab.com/waldemarlehner-hrw/ss-2022/shaderprogrammierung/abgabe/-/blob/master/src/data/projects/3d/lookAt/main.glsl",
    {
        u_skybox: skybox
    }
);

