import Project from "../../../Project";

import frag from "./main.glsl";
import skybox from "../lookAt/res/skybox.jpg"

export default new Project(
    "Basic Refraction", 
    "Implementierung eines primitiven Würfels mit Refraction", 
    "refraction-basic", 
    frag,
    "https://gitlab.com/waldemarlehner-hrw/ss-2022/shaderprogrammierung/abgabe/-/blob/master/src/data/projects/3d/basic-refraction/main.glsl",
    {
        u_skybox: skybox
    }
);

