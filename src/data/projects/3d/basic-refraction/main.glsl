#ifdef GL_ES
precision mediump float;
#endif
#pragma glslify: Camera = require("../lib/camera.glsl")
#pragma glslify: Ray = require("../lib/ray.glsl")
#pragma glslify: LIB_noise = require("../../../lib/noise.glsl");
#pragma glslify: LIB_intersectNormalizedCube = require("./intersectCube.glsl");

#pragma glslify: LIB_makeRayFromCamera = require("../lib/camera_ray.glsl");
#pragma glslify: getRandomColor = require("../../../lib/randomColor.glsl);


uniform vec2 u_resolution;
uniform float u_time;

uniform sampler2D u_skybox;

const float PI = 3.14159;



vec3 getSkyboxColor(Ray ray) {

    // Here the Ray Lookat-Direction gets converted into UV-Coords for a Skybox-Image
    vec3 dir = ray.dir;
    float u = atan(dir.x, dir.z) / (2.*PI);
    u = fract(u);
    float v = dir.y * .5 + .5;
    vec2 uv = vec2(u,v);
    // These UV-Coords are then used to sample the color from the Skybox texture
    vec4 tex = texture2D(u_skybox, uv);
    vec3 color = tex.xyz;
    
    
    // IF the ray is shooting "downwards", there is a possibility that we have a collision with the floor,
    // located at y = -2
    if(ray.dir.y < .0) {
        // Check where Ray intersects y=-2
        float dY = -2. - ray.pos.y; 
        vec3 rayNormalizedYDir = ray.dir / ray.dir.y; // now rayNormalized.y = 1.
        // This is basically the X and Z values of our inbound ray on y = -2. 
        // This acts as the "st" of the ground.
        vec2 collisionXZ = rayNormalizedYDir.xz * dY + ray.pos.xz;
        // Rotate the result so it is misaligned from the cube. We use Noise here. 
        // This gives a neat effect where the further away we are from the cube, the more chaotic the ground looks
        float angle = LIB_noise(collisionXZ * 0.4);
        // Apply simple rotation matrix onto the "st" of the floor.
        collisionXZ *= mat2( cos(angle), -sin(angle), sin(angle), cos(angle));

        // Now basically check if the resulting st has either component below 10.
        // This is used to generate the checkboard
        if(abs(collisionXZ.x) < 10. && abs(collisionXZ.y) < 10.) {
            collisionXZ = fract(collisionXZ*1.);
            bool isBlack = false;
            // Checkerboard is made by looking at the X and Y values. If (st mod 1) > .5 the "isBlack"-Flag
            // is flipped for the respective dimension.
            if(collisionXZ.x > .5) {
                isBlack = !isBlack;
            }
            if(collisionXZ.y > .5) {
                isBlack = !isBlack;
            }
            // Then set the color to either black or white.
            color = isBlack ? vec3(0.) : vec3(.8);

        } 
        // There are some additional borders at the edges. They are done basically the same way, just with a
        // slighthly increased bound.
        else if( abs(collisionXZ.x) < 10.25 && abs(collisionXZ.y) < 10.25) {
            color = vec3(.4, 0. ,0.);
        }else if( abs(collisionXZ.x) < 10.5 && abs(collisionXZ.y) < 10.5) {
            color = vec3(0.7255, 0.6431, 0.0);
        }else if( abs(collisionXZ.x) < 11. && abs(collisionXZ.y) < 11.) {
            color = vec3(0.0, 0.6275, 0.7843);
        }
    }

    
    return color;
}

struct CubeCollision {
    bool valid;
    float r;
    vec3 collision;
    vec3 normal;
};


CubeCollision getCollisionWithCube(Ray ray, vec3 cubeOrigin) {
    // This does not make use of Ray Marching. Instead, a "Bounding" Sphere is made for the Cube.
    // The ray is being normalized around the Cube… if the Cube has a x-length of 3, the ray
    // is compressed by a factor of 3 to get an "effective" Cube x-Length of 1. This is done for all
    // dimensions so everything is normalized around a unit-cube
    Ray transformedRay = ray;
    transformedRay.pos -= cubeOrigin;

    // Now get the distance between Ray and Origin (which represents the Cube's Origin)
    vec2 result = LIB_intersectNormalizedCube(transformedRay.pos, transformedRay.dir);
    if (result.y < .99) {
        // invalid
        return CubeCollision(false, 0., vec3(0.), vec3(0.));
    }
    vec3 collision = ray.pos + ray.dir * result.x;
    vec3 collisionRelativeToCube = cubeOrigin - collision;
    // Divide by cube dimensions to get "unit cube" vector.
    vec3 normal;
    {
        float currentComponentMax = 0.;
        if(abs(collisionRelativeToCube.x) > currentComponentMax) {
            currentComponentMax = abs(collisionRelativeToCube.x);
            normal = vec3(1., 0., 0.);
            if (collisionRelativeToCube.x < 0.) {
                normal = -normal;
            } 
        }
        if(abs(collisionRelativeToCube.y) > currentComponentMax) {
            currentComponentMax = abs(collisionRelativeToCube.y);
            normal = vec3(0., 1., 0.);
            if (collisionRelativeToCube.y < 0.) {
                normal = -normal;
            } 
        }
        if(abs(collisionRelativeToCube.z) > currentComponentMax) {
            currentComponentMax = abs(collisionRelativeToCube.z);
            normal = vec3(0., 0., 1.);
            if (collisionRelativeToCube.z < 0.) {
                normal = -normal;
            } 
        }
    }
    return CubeCollision(true, result.x, collision, normal);
}

// Debug override. -1 to deactivate. Keep within [0,1] when override is active.
const float OVERRIDE_WEIGHT = -1.;
// Inder of Refraction for the Cube.
const float INDEX_OF_REFRACTION = 1.6;

void main() {
    // Normalize st
    vec2 st = gl_FragCoord.xy / u_resolution;
    
    // Remap u_time so the rotation is slower
    float time_remap = u_time * 0.2;
    // Define the distance to the focus point.
    const float camDist = 5.;
    // "Build" a camera
    Camera cam = Camera(
        vec3(sin(time_remap) * camDist, sin(time_remap) * camDist * .4, cos(time_remap) * camDist) + vec3(0., 2.5, 0.),
        vec3(0), 
        60., 
        u_resolution.x/u_resolution.y
    );

    // Create a Camera Ray using the camera and st
    Ray ray = LIB_makeRayFromCamera(cam, st);

    // Look where the generated Ray would collide with the skybox.
    // Note that the floor is considered part of the skybox here
    vec3 outColor = getSkyboxColor(ray);

    const vec3 CUBE_POSITION = vec3(0., 1., 0.);
    /*
                           / <[D]
                          /
        |¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯ /¯¯¯¯|
        |              /      |
        |            /        |
        |          /          |
        |        [C]          |
        |      /              |
        |    /                |
        |  /                  |
        ¯¯/\¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
         /  \
        /    [B]
       [A]     \
        ^
    (start here)
    
    
    
    */

    // Do a collision check to see if we have a collision with the cube.
    // The ray here is [A]
    // result.valid is true if we did have a collision.
    CubeCollision result = getCollisionWithCube(ray, CUBE_POSITION);

    if (result.valid) {
        // Reflect off of the Surface of the Cube.
        // Because there is only one cube and the skybox in the scene, there is no need for another
        // collision check. So just get the Reflection Color from the Skybox.
        // ReflectColor is the Result from the [B]-Ray
        vec3 reflectColor = getSkyboxColor(Ray(result.collision, reflect(ray.dir, result.normal)));
        // Determine a weight that will be used to define how much reflection and refraction will be used.
        float weight = 1. -abs(dot(result.normal, ray.dir));
        weight *= weight;
        
        if(OVERRIDE_WEIGHT > -.5) {
            // This is used for debug purposes and should be active if OVERRIDE_WEIGHT is set. 
            // OVERRIDE_WEIGHT is expected to be within [0,1].
            weight = OVERRIDE_WEIGHT;
        }

        vec3 rayDirInsideCube = refract(ray.dir, result.normal, (1./INDEX_OF_REFRACTION));
        // Refract the [A]-ray. rayDirInsideCube becomes [C]
        Ray rayInsideCube = Ray(result.collision + rayDirInsideCube * .05, rayDirInsideCube);

        // Get the Collision "on the inside" (basically between [C] and [D])
        CubeCollision resultCollisionInside = getCollisionWithCube(rayInsideCube, CUBE_POSITION);
        if (resultCollisionInside.valid) {
            // Get point of Collision
            vec3 pointOfInsideCollision = resultCollisionInside.collision;
            vec3 rayDirOutside = refract(rayInsideCube.dir, resultCollisionInside.normal, INDEX_OF_REFRACTION);
            // Generate the outbound Ray ([D])
            Ray rayOutside = Ray(pointOfInsideCollision + rayDirOutside * .1, rayDirOutside);
            // Get the Color of the Ray that went through the Box [D] and mix it with the first reflected ray [B]. 
            //This will be the color sent to the Screen
            outColor = mix(reflectColor, getSkyboxColor(rayOutside), weight);
        }
    } 
    
    gl_FragColor = vec4(outColor, 1.);
}