// Code partially from Raytracing - Ray Object Intersections (https://fbe-nextcloud.rwu.de/s/NsiBcZGbkG4G89y)
bool collidesWithBoundingSphere(vec3 sphereCentre, float radius, vec3 rayOrigin, vec3 rayDir) {
    vec3 rayOriginToSphereCentre = rayOrigin - sphereCentre;
    float rayOriginToSphereCentreDotDir = dot(rayOriginToSphereCentre, rayDir);
    float root = rayOriginToSphereCentreDotDir * rayOriginToSphereCentreDotDir - (dot(rayOriginToSphereCentre, rayOriginToSphereCentre) - radius*radius);
    return root > 1e-5;
}



// Uses
// This assumes that the Cube is positioned at the Origin and ranges from -1 to 1 on all 3 axes
// Also a small warning: This is not really an elegant solution. 
// Using an SDF would be waaaaaay shorter. But I just thought I would give it a try anyways.
vec2 LIB_intersectNormalizedCube(vec3 rayStart, vec3 rayDir) {
    float rayLen = length(rayDir);
    // Check if the Cube is inside the "bounding sphere" of the cube. If not, the next steps can be simply skipped.
    if( !collidesWithBoundingSphere(vec3(0.), 2., rayStart, rayDir)) {
        return vec2(0.);
    }

    // These values are normalized towards a specific component of the vector.
    // so normalizeX is set up so that X is 1 while still having an identical direction.
    vec3 rayDirNormalizedX = rayDir / rayDir.x;
    vec3 rayDirNormalizedY = rayDir / rayDir.y;
    vec3 rayDirNormalizedZ = rayDir / rayDir.z;
    
    // Set r to some ludricrously high value. Basicually positive infinity – as we try to 
    // optimize and get the r as low as possible here.
    float r = 1e20;
    int collision = 0;

    // Now, basically for each side the of the Cube a Plane is set up.
    // For example for the Top side, check where X and Z are for Y = 1. If they are inside the "unit cube" (x,y in [-1,1])
    // we have a collision with the face. From then on check if this is the closest collision so far. If yes, update.


    // Top Side
    {
        // difference between rayStart and the Top side (y=1)
        float dY = 1. - rayStart.y;
        vec2 xz = dY * rayDirNormalizedY.xz + rayStart.xz;
        if (abs(xz.x) <= 1. && abs(xz.y) <= 1.) {
            // Collision happened on Unit-Cube -> Is relevant
            vec3 new_pos = vec3(xz.x, 1., xz.y);
            float new_r = distance(rayStart, new_pos) / rayLen;
            if (new_r > .0 && new_r < r) {
                r = new_r;
                collision = 1;
            }
        }
    }
    // Bottom Side
    {
        // difference between rayStart and the Top side (y=1)
        float dY = -1. - rayStart.y;
        vec2 xz = dY * rayDirNormalizedY.xz + rayStart.xz;
        if (abs(xz.x) <= 1. && abs(xz.y) <= 1.) {
            // Collision happened on Unit-Cube -> Is relevant
            vec3 new_pos = vec3(xz.x, -1., xz.y);
            float new_r = distance(rayStart, new_pos) / rayLen;
            if (new_r > .0 && new_r < r) {
                r = new_r;
                collision = 2;
            }
        }
    }
    // Left Side
    {
        // difference between rayStart and the Top side (y=1)
        float dX = -1. - rayStart.x;
        vec2 yz = dX * rayDirNormalizedX.yz + rayStart.yz;
        if (abs(yz.x) <= 1. && abs(yz.y) <= 1.) {
            // Collision happened on Unit-Cube -> Is relevant
            vec3 new_pos = vec3(-1, yz.x, yz.y);
            float new_r = distance(rayStart, new_pos) / rayLen;
            if (new_r > .0 && new_r < r) {
                r = new_r;
                collision = 2;
            }
        }
    }
    // Right Side
    {
        // difference between rayStart and the Top side (y=1)
        float dX = 1. - rayStart.x;
        vec2 yz = dX * rayDirNormalizedX.yz + rayStart.yz;
        if (abs(yz.x) <= 1. && abs(yz.y) <= 1.) {
            // Collision happened on Unit-Cube -> Is relevant
            vec3 new_pos = vec3(1, yz.x, yz.y);
            float new_r = distance(rayStart, new_pos) / rayLen;
            if (new_r > .0 && new_r < r) {
                r = new_r;
                collision = 2;
            }
        }
    }
    // Back Side
    {
        // difference between rayStart and the Top side (y=1)
        float dZ = 1. - rayStart.z;
        vec2 xy = dZ * rayDirNormalizedZ.xy + rayStart.xy;
        if (abs(xy.x) <= 1. && abs(xy.y) <= 1.) {
            // Collision happened on Unit-Cube -> Is relevant
            vec3 new_pos = vec3(xy.x, xy.y, 1.);
            float new_r = distance(rayStart, new_pos) / rayLen;
            if (new_r > .0 && new_r < r) {
                r = new_r;
                collision = 2;
            }
        }
    }
    // Front Side
    {
        // difference between rayStart and the Top side (y=1)
        float dZ = -1. - rayStart.z;
        vec2 xy = dZ * rayDirNormalizedZ.xy + rayStart.xy;
        if (abs(xy.x) <= 1. && abs(xy.y) <= 1.) {
            // Collision happened on Unit-Cube -> Is relevant
            vec3 new_pos = vec3(xy.x, xy.y, -1.);
            float new_r = distance(rayStart, new_pos) / rayLen;
            if (new_r > .0 && new_r < r) {
                r = new_r;
                collision = 2;
            }
        }
    }



    return vec2(r, float(collision));
}

#pragma glslify: export(LIB_intersectNormalizedCube)