#pragma glslify: Camera = require("./camera.glsl");
#pragma glslify: Ray = require("./ray.glsl");

Ray LIB_createRayForCameraNdc(Camera cam, vec2 ndc) {
    float xOffset = tan(radians(cam.fov)/2.);
    float yOffset = xOffset / cam.ar;

    vec2 pixelPosition = vec2(xOffset, yOffset) * ndc;

    vec3 newForward = normalize(cam.focus - cam.position);
    // See https://computergraphics.stackexchange.com/a/10764
    vec3 up = vec3(0., 1., 0.);
    vec3 newRight = (cross(up, newForward));
    vec3 newUp = (cross(newForward, newRight));

    vec3 direction = pixelPosition.x * newRight + 
                     pixelPosition.y * newUp + 
                     newForward;


    return Ray(cam.position, normalize(direction));
}

Ray LIB_createRayForCamera(Camera cam, vec2 st) {
    // map st (0->1) to NDC (-1 -> 1)
    vec2 ndc = (st * 2.) - vec2(1);
    return LIB_createRayForCameraNdc(cam, ndc);
}

#pragma glslify: export(LIB_createRayForCamera)