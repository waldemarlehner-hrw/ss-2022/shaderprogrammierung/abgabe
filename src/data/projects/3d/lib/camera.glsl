struct Camera {
    vec3 position;
    vec3 focus;
    float fov;
    float ar;
};

#pragma glslify: export(Camera)