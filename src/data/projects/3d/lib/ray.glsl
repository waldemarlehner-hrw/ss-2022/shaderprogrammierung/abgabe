struct Ray {
    vec3 pos;
    vec3 dir;
};

#pragma glslify: export(Ray);