import Project from "../../../Project";

import frag from "./cellular1.glsl";
export default new Project(
    "Cellular Noise", 
    "Implementierung von Cellular Noise unter Verwendung der Surrounding-Neighbour-Optimierung.", 
    "cellular1", 
    frag,
    "https://gitlab.com/waldemarlehner-hrw/ss-2022/shaderprogrammierung/abgabe/-/blob/master/src/data/projects/2d/cellular-1/cellular1.glsl"
);

