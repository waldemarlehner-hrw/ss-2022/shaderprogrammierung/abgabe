#ifdef GL_ES
precision mediump float;
#endif

#pragma glslify: LIB_random = require("../../../lib/random.glsl")
#pragma glslify: LIB_noise = require("../../../lib/noise.glsl")

const bool SHOW_POINTS = true;
//
// 0 -> Color
// 1 -> Distance
// 2 -> Dist Inv
// 3 -> Dist Stripe
//



uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;
#pragma glslify: LIB_normalizeInput = require("../../../lib/normalizeInput.glsl, u_resolution=u_resolution)




vec2 calculatePositionForIndex(ivec2 index) {
    vec2 localPos = vec2(LIB_random(float(index.x * index.y+index.y)+.434), LIB_random(float(index.y*index.x+index.x)+.9412)) * (.5+.5*sin(u_time));
    
    vec2 globalPos = localPos + (vec2(index));
    return globalPos;
}

vec3 calculateColorForIndex(ivec2 index) {
    return vec3(LIB_random(vec2(index)+.1), LIB_random(vec2(index)+.2), LIB_random(vec2(index)+.3));
}


struct Point {
    vec2 pointPos;
    vec3 color;
};

Point buildPoint(ivec2 index) {
    Point point = Point(
        calculatePositionForIndex(index), 
        calculateColorForIndex(index)
    );
    return point;
}


void main() {
    // Normalize ST and Mouse to have their origin in the centre.
    vec2 st = LIB_normalizeInput(gl_FragCoord.xy);
    vec2 mx = LIB_normalizeInput(u_mouse);


    // Select the desired render mode.
    // Based on in which corner the mouse is in, a different 
    // mose is chosen.
    int RENDER_MODE = 0;
    if (mx.x > .0) {
        RENDER_MODE += 1;
    }
    if (mx.y > .0) {
        RENDER_MODE += 2;
    }

    // Define Pattern Density. It is set by the Mouse Manhattan Distance to the screen centre
    float PatternDensity = (abs(mx.x) + abs(mx.y)) * 10.;

    

 
    // "Patternize st"

    st *= PatternDensity;
    ivec2 patternIndex = ivec2(floor(st));

    // Evaluate own and surrounding points
    Point points[9];
    
    points[0] = buildPoint(patternIndex + ivec2(-1,1));
    points[1] = buildPoint(patternIndex + ivec2(0,1));
    points[2] = buildPoint(patternIndex + ivec2(1,1));
    points[3] = buildPoint(patternIndex + ivec2(-1,0));
    points[4] = buildPoint(patternIndex + ivec2(0,0));
    points[5] = buildPoint(patternIndex + ivec2(1,0));
    points[6] = buildPoint(patternIndex + ivec2(-1,-1));
    points[7] = buildPoint(patternIndex + ivec2(0,-1));
    points[8] = buildPoint(patternIndex + ivec2(1,-1));


    float dist = 1e20;
    vec3 color;
    for(int i = 0; i < 9; i++) {
        // Iterate over all neighbouring points (and own point) and find the minimum point.
        // Its Color and Distance is then stored.
        Point p = points[i];
        float distForP = distance(p.pointPos, st);
        if(distForP < dist) {
            dist = distForP;
            color = p.color;
        }
    }

    if(dist < .02 && SHOW_POINTS) {
        // Draw Black Point for centre of Point
        color = vec3(0);
    }

    if(RENDER_MODE==1 || RENDER_MODE==2) {
        // Distance Based Modes (Distance Field and Inversed Distance Field)
        float val = dist;

        if( RENDER_MODE == 2) {
            // Inverse Colour when in Inversed Distance Field Mode
            val = 1. - val;
        }
        color = vec3(val);
    }

    if(RENDER_MODE==3) {
        // if in "Ringed" Mode, draw rings from Centre using sine-Function.
        bool darken = sin(dist*50.) < 0.;
        if (darken) {
            color *= .7;
        }
    }


    gl_FragColor = vec4(color, 1.);
}