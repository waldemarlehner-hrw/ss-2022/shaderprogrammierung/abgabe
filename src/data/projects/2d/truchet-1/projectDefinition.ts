import Project from "../../../Project";

// @ts-ignore
import frag from "./truchet-1.glsl";

export default new Project(
    "Truchet 1", 
    "Implementierung von Truchet Tiles im Greyscale-Look", 
    "truchet1", 
    frag,
    "https://gitlab.com/waldemarlehner-hrw/ss-2022/shaderprogrammierung/abgabe/-/blob/master/src/data/projects/2d/truchet-1/truchet-1.glsl"
);


