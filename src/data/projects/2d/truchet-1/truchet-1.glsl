#ifdef GL_ES
precision mediump float;
#endif

#pragma glslify: LIB_random = require("../../../lib/random.glsl)

const float PI = 3.14159;

// Define how many tiles along X-Axis are present
const float PatternDensity = 20.;

uniform vec2 u_resolution;
uniform float u_time;


// Apply a rotation matrix on an input vector by an input angle
vec2 rotateLocal2D(vec2 vector, float angle) {
    vector -= .5; // move anker to centre
    vector *= mat2(
        cos(angle), -sin(angle),
        sin(angle), cos(angle)
    );
    vector += .5; // restore anker
    return vector;
}


void main() {
    vec2 st = gl_FragCoord.xy / u_resolution;
    // Correction of ST
    float ar = u_resolution.y / u_resolution.x;
    st.y *= ar;
    st.x -= .5;
    st.y -= .5 * ar;
    
    // "Patternize st"
    st *= PatternDensity;
    ivec2 patternIndex = ivec2(floor(st));
    st = vec2(mod(st,1.));
    //

    int timeMod = int(u_time*2.);

    // Draw the tile
    vec3 colorMask;
    //int index = patternIndex.x * timeMod + patternIndex.y * patternIndex.y * timeMod;
    //index = int(floor(mod(float(index), 4.)));
    int index = int(LIB_random(vec2(patternIndex) + vec2(timeMod)) * 4.);

    if(index == 0) {
        // Keep as is – no rotation 0°
        colorMask = vec3(0.5412, 0.5412, 0.5412);
    }
    else if(index == 1) {
        st = rotateLocal2D(st, .5*PI); // 90°
        colorMask = vec3(0.4314, 0.4314, 0.4314);

    }
    else if(index == 2) {
        st = rotateLocal2D(st, PI); // 180°
        colorMask = vec3(0.2745, 0.2745, 0.2745);

    }
    else if(index == 3) {
        st = rotateLocal2D(st, 1.5*PI); // 270°
        colorMask = vec3(0.1725, 0.1725, 0.1725);
    }

    // if "active" half of rectangle this is 1, else this is 0.
    // eval is then multiplied with colorMask to result in either correct color or black.
    float eval = step(1., st.x+st.y);
    gl_FragColor = vec4(eval * colorMask, 1.);
}