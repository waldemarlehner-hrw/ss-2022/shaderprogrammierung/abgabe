#ifdef GL_ES
precision mediump float;
#endif
#pragma glslify: LIB_noise = require("../../../lib/noise.glsl)
#pragma glslify: hsl2rgb = require(glsl-hsl2rgb)

const float PatternDensity = 16.;




uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;
#pragma glslify: LIB_normalizeInput = require("../../../lib/normalizeInput.glsl, u_resolution=u_resolution)


mat2 rotate2d(float angle) {
    return mat2(
        cos(angle), -sin(angle),
        sin(angle), cos(angle)
    );
}

/**
Warning: Make sure iteration Count is < ITERATION_COUNT_MAX
*/
const int ITERATION_COUNT_MAX = 10;

float fbm(vec2 st, int iterationCount) {
    float val = 0.0;
    float amplitude = 1./2.;
    for( int i = 0; i < ITERATION_COUNT_MAX; i++) {
        if (i >= iterationCount) {
            break; // trick to allow for "dynamic" Loops
        }
        st *= rotate2d(val + u_time * .05);
        val += amplitude * LIB_noise(st);
        amplitude /= 2.; // amplitude is 2^(-i-1)
        st *= 2.; // st multiplier is 2^(i)
    }
    return val;
}



void main() {
    // FragCoord and Mouse both normalized and centered
    vec2 st = LIB_normalizeInput(gl_FragCoord.xy);
    vec2 mx = LIB_normalizeInput(u_mouse);
    // make moving mouse move canvas
    st -= mx;


    // "Patternize st"
    st *= PatternDensity;
    ivec2 patternIndex = ivec2(floor(st));



    // uncomment for index visualization
    if(!true) {
        bool isBlack = false;
        vec2 temp = fract(vec2(patternIndex) / 2.);
        if (temp.x > .49) {
            isBlack = !isBlack;
        }
        if (temp.y > .49) {
            isBlack = !isBlack;
        }
        gl_FragColor = vec4(isBlack ? 0. : 1.);
        return;
    }
    //


        
    
    // I had an iteration counter here that would change the octaves here.
    // i decided to remove it as it looked bad. Feel free to uncomment the code below:
    //int ITERATION_COUNT = 2+int(((sin(u_time) + 1.) * float(ITERATION_COUNT_MAX) / 2.));
    int ITERATION_COUNT = ITERATION_COUNT_MAX;


    float result = fbm(st, ITERATION_COUNT);
    float hue_offset = u_time * 5.;
    // map result from [0,1] (0, 360) to a hue range
    float hue = mix(240.+hue_offset, 280.+hue_offset, result);
    float saturation = 1.;
    float value = result * .5;
    // convert from hsl to rgb
    vec3 result_rgb = hsl2rgb(vec3(fract(hue/360.), saturation, value));
    //result_rgb = hsl2rgb(vec3(51./360., 1., .9));

    gl_FragColor = vec4(result_rgb, 1.);
    return;

    
}