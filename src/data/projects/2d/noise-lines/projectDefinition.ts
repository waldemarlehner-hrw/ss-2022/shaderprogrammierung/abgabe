import Project from "../../../Project";

// @ts-ignore
import frag from "./noise-lines.glsl";

export default new Project(
    "Noise Lines", 
    "Bei Experiementen mit Noise kam dieses Ergebnis raus.", 
    "noiselines", 
    frag,
    "https://gitlab.com/waldemarlehner-hrw/ss-2022/shaderprogrammierung/abgabe/-/blob/master/src/data/projects/2d/noise-lines/noise-lines.glsl"
);

