import Project from "../../../Project";

// @ts-ignore
import frag from "./main.glsl";

export default new Project(
    "Parallax", 
    "Eine 2D-Szene welche von dem Parallax-Effekt gebraucht macht. Der Shader reagiert auf die Mausposition.", 
    "parallax", 
    frag,
    "https://gitlab.com/waldemarlehner-hrw/ss-2022/shaderprogrammierung/abgabe/-/blob/master/src/data/projects/2d/parallax/main.glsl"
);

