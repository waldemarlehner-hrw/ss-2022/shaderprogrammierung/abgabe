#ifdef GL_ES
precision mediump float;
#endif

#pragma glslify: LIB_noise = require("../../../lib/noise.glsl)
#pragma glslify: LIB_random = require("../../../lib/random.glsl)
#pragma glslify: hsl2rgb = require(glsl-hsl2rgb)

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;
#pragma glslify: LIB_normalizeInput = require("../../../lib/normalizeInput.glsl, u_resolution=u_resolution)


// Define all the different parameters
const int HILL_COUNT = 15;

const float HILL_AMPLITUDE_MULTIPLIER = .3;
const float NEAR_HILL_PARALLAX_MULT = 2.;
const float FAR_HILL_PARALLAX_MULT = .1;
const float HUE_NEAR = 250.;
const float HUE_FAR = 10.;
const float VALUE_NEAR = .6;
const float VALUE_FAR = .5;
const float SATURATION_NEAR = 1.;
const float SATURATION_FAR = .7;

const vec2 ST_BOUNDS = vec2(0., 1.);
float getBaseLine(float fraction, float parallaxMult, float mx_y) {
    // Note that we start at Upper Bound and go to Lower Bound. This is because the "closer" parts
    // are at the bottom of the canvas
    float baselineYNoParallax = mix(ST_BOUNDS.y, ST_BOUNDS.x, fraction);
    return baselineYNoParallax + mx_y * parallaxMult;
}

// Basically just evaluates an FBM-Noise at (x,a)
float evaluateHeight(float x, int a) {
    float value = 0.;
    // FBM-Noise is used to determine the Height of this Hill
    const int ITERATION_COUNT = 10;
    for(int i = 1; i <= ITERATION_COUNT; i++) {
        if(i >= ITERATION_COUNT) {
            break;
        }
        float y = LIB_random(float(a));
        // Noise Fidelity grows exponentially
        vec2 noiseInput = vec2(x, y) * float(i*i);
        value += 1./float(i) * LIB_noise( noiseInput );
    }
    
    value /= log2(float(ITERATION_COUNT));
    return value;
}




void main() {
    // FragCoord and Mouse both normalized. Mouse gets centered, ST does not.
    vec2 st = gl_FragCoord.xy / u_resolution; // Normalized for y.
    //st.y is always inbetween 0 and 1
    vec2 mx = LIB_normalizeInput(u_mouse * vec2(-1., 1.));

 

    // Here, each "layer" is being iterated through from back to front.
    // The last index remaining will be used to apply the colour.
    int selectedIndex = -1;
    for(int i = 0; i < HILL_COUNT; i++) {
        // value in range [0; 1]
        float currentFraction = float(i) / (float(HILL_COUNT) - 1.);
        // Linearized Value between FAR_HILL_PARALLAX_MULT and NEAR_HILL_PARALLAX_MULT. 
        float parallaxMult = mix(FAR_HILL_PARALLAX_MULT, NEAR_HILL_PARALLAX_MULT, currentFraction);
        // X-Value used for evaluating the Hill height
        float xToEval = st.x + mx.x * parallaxMult + u_time * .1 * parallaxMult;
        // Result from FBM evaluation
        float eval = evaluateHeight(xToEval, i);
        //eval = 0.; // TODO: remove once debugging done
        // Baseline defines at which st.y eval = 0 should reside.
        float baseLine = getBaseLine(currentFraction, parallaxMult, mx.y);
        // yHeight is the final result for the height of the hill at the current position.
        // this includes baseline and parallax.
        float yHeight = baseLine + eval * HILL_AMPLITUDE_MULTIPLIER;
        if (yHeight > st.y) {
            // if st.y is "below" yHeight, we are inside the Hill -> use the Colour of the Hill.
            selectedIndex = i;
        }
    }

    vec3 color = vec3(0);
    if(selectedIndex > 0) {
        float f = float(selectedIndex) / float(HILL_COUNT - 1);
        float hue = mix(HUE_FAR, HUE_NEAR, f);
        float saturation = mix(SATURATION_FAR, SATURATION_NEAR, f);
        float value = mix(VALUE_FAR, VALUE_NEAR, f);
        
        // Farbe wird mit externer Library von HSV-Farbraum in RGB Konvertiert
        color = hsl2rgb(hue/360., saturation, value);

    } else {
        // Sky
        vec3 top = vec3(2.,50.,209.) / 255.;
        vec3 bottom = vec3(55.,299.,233.) / 255.;
        float fraction = mix(-1. ,1., st.y);
        color = mix(bottom, top, fraction);
    }
 

    gl_FragColor = vec4(color, 1.);


}