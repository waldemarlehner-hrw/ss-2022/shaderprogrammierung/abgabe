#ifdef GL_ES
precision mediump float;
#endif

#pragma glslify: LIB_random = require("../../../lib/random.glsl)

const float PatternDensity = 16.;
const int BlackWeight = 5;
const int PrimaryWeight = 3;
const int SecondaryWeight = 2;

const vec3 PrimaryColor = vec3(1.0, 0.6, 0.0);
const vec3 SecondaryColor = vec3(1.,1.,0.);



uniform vec2 u_resolution;
uniform float u_time;

void main() {
    vec2 st = gl_FragCoord.xy / u_resolution;
    // Correction of ST
    float ar = u_resolution.y / u_resolution.x;
    st.y *= ar;
    st.y -= .5 * ar;
    

    // "Patternize st"
    st *= PatternDensity;
    ivec2 patternIndex = ivec2(floor(st));

    // The PatternIndex is 0-->n
    // move to -n/2<->n/2
    // This way the image is mirrored along the X-Axis
    bool isEven = int(mod(float(PatternDensity), 2.)) == 0;

    int halfIndex = int(PatternDensity / 2.) ;
    if(patternIndex.x < halfIndex) {
        patternIndex.x = halfIndex - (patternIndex.x - halfIndex) ;
        if(isEven) {
            // This is only relevant if the PatternDensity is a multiple of 2 (e.g. 10, 14, 20).
            // this will correct the left side to move 1 texel to the left to have proper mirroring.
            patternIndex.x -= 1;
        }
    }

    // uncomment for index visualization
    if(!true) {
        // Just Debug Code colouring each Texel separately.
        gl_FragColor = vec4(1) * float(patternIndex.x) / PatternDensity + mod(float(patternIndex.y), 2.) * .02;
        return;
    }
    //

    // Get a "Hash" for the Pattern Index. 
    // The Hash takes u_time as a salt to create a new picture
    float randomEval = LIB_random(vec2(patternIndex) + vec2(floor(u_time * 0.5)));
    // uncomment for random eval visualization
    if(!true) {
        gl_FragColor = vec4(vec3(randomEval), 1.);
        return;
    }
    // Map random (0->1) to (0->All Weights)
    randomEval *= float(BlackWeight + PrimaryWeight + SecondaryWeight);


    vec3 color;
    // The random value was distributed according to the weights.
    if(randomEval < float(BlackWeight)) {
        // Handle as Black Tile
        color = vec3(0);
    }
    else if(randomEval < float(BlackWeight + PrimaryWeight)) {
        // Handle as Primary Tile
        color = PrimaryColor;
    }
    else {
        // Handle as Secondary Tile
        color = SecondaryColor;
    }

    gl_FragColor = vec4(color, 1.);
    return;

}