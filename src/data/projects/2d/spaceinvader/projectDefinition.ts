import Project from "../../../Project";

// @ts-ignore
import frag from "./main.glsl";

export default new Project(
    "Space Invaders", 
    "Implementierung von Space Invadern mit GLSL", 
    "spaceinvader", 
    frag,
    "https://gitlab.com/waldemarlehner-hrw/ss-2022/shaderprogrammierung/abgabe/-/blob/master/src/data/projects/2d/spaceinvader/main.glsl"
);

