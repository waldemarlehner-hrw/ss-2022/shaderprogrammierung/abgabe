import Project from "../../../Project";

// @ts-ignore
import frag from "./spiral.glsl";

export default new Project(
    "Polar Spiral", 
    "Eine einfache Animation bei welcher über die Polarkoordinaten und Abstand zum Ursprung die Farbe bestimmt wird", 
    "polar-spiral", 
    frag,
    "https://gitlab.com/waldemarlehner-hrw/ss-2022/shaderprogrammierung/abgabe/-/blob/master/src/data/projects/2d/polar-spiral/spiral.glsl"
);

