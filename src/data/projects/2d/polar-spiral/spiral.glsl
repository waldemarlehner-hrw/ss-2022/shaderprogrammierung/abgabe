#ifdef GL_ES
precision mediump float;
#endif

const float PI = 3.14159;
const int ARMS = 8;

uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;
#pragma glslify: LIB_normalizeInput = require("../../../lib/normalizeInput.glsl, u_resolution=u_resolution)






void main() {
    // FragCoord normalized and centered
    vec2 st = LIB_normalizeInput(gl_FragCoord.xy);
    vec2 mx = LIB_normalizeInput(u_mouse);


    float SIZE_END_MULTIPLIER = (-mx.y+1.) * .5 * 40.;
    float CURVATURE_MULTIPLIER = mx.x * 10.;

    
    vec2 st_normalized = normalize(st);
    float polarCoordDeg = atan(st_normalized.x, st_normalized.y);
    // Subtracting u_time here will result in a spin. set positive for
    // counter-clockwise spin
    polarCoordDeg -= u_time;

    float distToCentre = length(st);
    distToCentre *= 2.;

    // Multiply Arm-Count and Result from atan – this will create the arms WITHOUT curvature
    // Add distToCentre * CURVATURE_MULTIPLIER into cos input for curvature.
    float value = cos((polarCoordDeg+distToCentre * CURVATURE_MULTIPLIER) * float(ARMS));
    // Reduce value based on distance. This will result in the Arms having an End. Also multiply
    // with SIZE_END_MULTIPLIER to manipulate arm length with mouse.
    value -= distToCentre * SIZE_END_MULTIPLIER / 10.;

    if (value < 0.) {
        value = 0.; // Not really needed, but to keep things clean.
    }

    gl_FragColor = vec4(vec3(value), 1.);
}