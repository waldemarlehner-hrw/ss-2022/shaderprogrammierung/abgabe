#ifdef GL_ES
    precision mediump float;
#endif

#pragma glslify: LIB_noise = require("../../../lib/noise.glsl)
#pragma glslify: LIB_random = require("../../../lib/random.glsl)
#pragma glslify: LIB_randomColor = require("../../../lib/randomColor.glsl)


uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;
#pragma glslify: LIB_normalizeInput = require("../../../lib/normalizeInput.glsl, u_resolution=u_resolution)



float noiseStacked(vec2 pos) {
    // Stacking Noises over each other. 
    // Each new Layer has finer detail and a lower amplitude. (see FBM)
    const int ITERATION_COUNT = 6;
    float value = 0.;
    for (int i = 0; i < ITERATION_COUNT; i++) {
        vec2 probe = pos * float(i+1) * 4. + LIB_random(float(i)) * 20.;
        float val = LIB_noise(probe);
        value += (val / float(i+3));
    }
    // Multiply with itself. No other reason that I liked the look of it more :)
    value *= value;
    return value;
}

void main() {
    // FragCoord and Mouse both normalized and centered
    vec2 st = LIB_normalizeInput(gl_FragCoord.xy);
    vec2 mx = LIB_normalizeInput(u_mouse);
    // Defines the coordinate of the current screen centre
    vec2 current_zero = u_time * vec2(0.1, .2) * 0.1- mx;

    // Define Colors and Parallax Factor
    const vec3 backgroundColor = vec3(.05);
    const vec3 ring1Color = vec3(1., .0, .0);
    const vec3 ring2Color = vec3(1., 1.0,.0);
    const vec3 ring3Color = vec3(1., .0, 1.);
    const float ring1Parallax = 1.;
    const float ring2Parallax = .9;
    const float ring3Parallax = .8;

    vec3 color = backgroundColor;
    // Each Ring calculates its own Frac Coords.
    // The get manipulated by the distance to Zero (Screen Centre) and the Parallax Factor
    // if the Noise evaluates to be within this ring, the color gets overridden.
    {
        // Ring 3 [.2; .3]
        vec2 r3_st = current_zero - st / ring3Parallax;
        float eval = noiseStacked(r3_st);
        if (eval > .2 && eval < .3) {
            color = ring3Color;
        }
    }
    {
        // Ring 2 [.4; .5]
        vec2 r2_st = current_zero - st / ring2Parallax;
        float eval = noiseStacked(r2_st);
        if (eval > .4 && eval < .5) {
            color = ring2Color;
        }
    }
    {
        // Ring 1 [.55; .7]
        vec2 r1_st = current_zero - st / ring1Parallax;
        float eval = noiseStacked(r1_st);
        if (eval > .55 && eval < .7) {
            color = ring1Color;
        }
    }
    gl_FragColor = vec4(color, 1.0);
} 