import Project from "../../../Project";

import frag from "./main.glsl";

export default new Project(
    "Noise Parallax Stack", 
    "Übereinander gelegte Noise-Layer mit Parallax", 
    "noise-parallax", 
    (frag),
    "https://gitlab.com/waldemarlehner-hrw/ss-2022/shaderprogrammierung/abgabe/-/blob/master/src/data/projects/2d/noise-parallax-stack/main.glsl"    
);

