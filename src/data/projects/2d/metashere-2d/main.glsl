#ifdef GL_ES
precision mediump float;
#endif


uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;

// Anything below that value will be painted black. Anything above is part of the Metasphere Shape.
const float EVAL_CUTOFF = 5.;

struct MetaSphere {
    float magnitude; // The "size" of this Metaball. Defines the Multiplier in the Sum-Function.
                     // can be negative to turn into a "negative" Metaball
    vec3 color;
    vec2 positionalCenter; // Each Metasphere circles around on a track. This is the tracks centre
    vec2 positionalMultiplier; // ^ This is the spheres track "edge". 
                               // It is defined by two multiplier which set an XY-Aligned Ellipsis.
    float movementSpeed;  // Multiplier with u_time for this Metasphere
    float movementOffset; // Offset for where on the "track" the Sphere should start
};

// Calculate the Spheres Position – dependent on u_time
vec2 getPositionOfMetaSphere(MetaSphere s) {
    vec2 origin = s.positionalCenter;
    float timeInput = s.movementOffset + u_time * s.movementSpeed;
    float xMul = cos(timeInput) * s.positionalMultiplier.x;
    float yMul = sin(timeInput) * s.positionalMultiplier.y;
    return origin + vec2(xMul, yMul);
}

// Evaluate a Single Ball using the Cubic Delta-Function
float eval(MetaSphere sphere, vec2 st, vec2 spherePos) {
    // see https://www.desmos.com/calculator/evgwgorgvj
    float dX = spherePos.x - st.x;
    float dY = spherePos.y - st.y;
    return sphere.magnitude / sqrt(dX*dX + dY*dY);
}

void main() {
    vec2 st = gl_FragCoord.xy / u_resolution;
    // Correction of ST
    float ar = u_resolution.y / u_resolution.x;
    st.y *= ar;
    st.x -= .5;
    st.y -= .5 * ar;

    const int METASPHERE_COUNT = 4;
    MetaSphere spheres[METASPHERE_COUNT];
    // Define Metasphere Properties for each instance
    spheres[0] = MetaSphere(.5, vec3(1., 0.,0.), vec2(.0, 0.), vec2(.1), 2., .5);
    spheres[1] = MetaSphere(.3, vec3(0.0667, 0.0, 1.0), vec2(.1, 0.1), vec2(.13, 0.), 1., .4);
    spheres[2] = MetaSphere(.2, vec3(1., 1.,0.), vec2(-.1, 0.), vec2(.2), 1.337, .0);
    spheres[3] = MetaSphere(-.4, vec3(1., 1.,0.), vec2(.1, 0.), vec2(.1), 1., .0);


    // Sum of the evaluation of all Metaspheres.
    // Is used to determine if the Function evaluated above the Cutoff.
    float sum = 0.;
    vec3 color = vec3(1.);

    // These 2 variables are used to decide if the centre dot should be red or green.
    // if the closes Sphere has a negative amplitude, it will be red, else it will be green.
    float distanceToSphere = 1e10;
    bool isDistPositive = false;

    for(int i = 0; i < METASPHERE_COUNT; i++) {
        MetaSphere sphere = spheres[i];
        vec2 currentPosition = getPositionOfMetaSphere(sphere);

        // Evaluate the current Metasphere and add to the total sum
        float result = eval(sphere, st, currentPosition);
        sum += result;
        if(sphere.magnitude > 0.) {
            // Mix the colour of the current Metasphere into the "summed" color. 
            // The colour is weighted on the own sphere's magnitude. 
            color = mix(color, spheres[i].color, result/sum);
        }

        // Logic to determine the closest Sphere for drawing the red/green dots.
        float newDistToSphere = distance(getPositionOfMetaSphere(sphere), st);
        if(newDistToSphere < distanceToSphere) {
            distanceToSphere = newDistToSphere;
            isDistPositive = sphere.magnitude > 0.;
        }
    }

    if (distanceToSphere < .002) {
        // Draw Red or Green Dot
        vec3 color = isDistPositive ? vec3(0., 1., 0.) : vec3(1., 0., 0.);
        gl_FragColor = vec4(color, 1.);
    }
    else if (sum > EVAL_CUTOFF) {
        // Draw the Color of the Metaball
        gl_FragColor = vec4( color , 1.);
    }
    else {
        // Draw Black because value is below cutoff.
        gl_FragColor = vec4(vec3(0.), 1.);
    }

}