import Project from "../../../Project";

// @ts-ignore
import frag from "./main.glsl";

export default new Project(
    "Metaspheres (2D)", 
    "Implementierung von Metaspheres. Im Beispiel sind 3 Positive (grün) und 1 Negative (rot) Metaspheres", 
    "metasphere2d", 
    frag,
    "https://gitlab.com/waldemarlehner-hrw/ss-2022/shaderprogrammierung/abgabe/-/blob/master/src/data/projects/2d/metashere-2d/main.glsl"
);

