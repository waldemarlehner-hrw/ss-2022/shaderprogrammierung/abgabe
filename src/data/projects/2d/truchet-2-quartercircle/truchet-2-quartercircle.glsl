#ifdef GL_ES
precision mediump float;
#endif

const float PI = 3.14159;

const float PatternDensity = 16.;


uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;
#pragma glslify: LIB_normalizeInput = require("../../../lib/normalizeInput.glsl, u_resolution=u_resolution)
#pragma glslify: LIB_random = require("../../../lib/random.glsl")



// Will return 1 if within the quarter circle, else 0.
float evalCircle(float radius, vec2 st) {
    return step(length(st), radius);
}

vec2 rotateLocal2D(vec2 vector, float angle) {
    vector -= .5; // move anker to centre
    vector *= mat2(
        cos(angle), -sin(angle),
        sin(angle), cos(angle)
    );
    vector += .5; // restore anker
    return vector;
}



void main() {
    // FragCoord normalized and centered
    vec2 st = LIB_normalizeInput(gl_FragCoord.xy);
    vec2 mx = LIB_normalizeInput(u_mouse);

    // Apply modification over time to create scrolling effect.
    // note that it needs to be applied to both mouse(mx) and st so the distance-Effect can still line up.
    st += vec2(.1) * u_time;
    mx += vec2(.1) * u_time;
    

    // "Patternize st"
    st *= PatternDensity;
    // do the same with mouse so that it lines up correctly.
    mx *= PatternDensity;
    ivec2 patternIndex = ivec2(floor(st));
    vec2 local_st = vec2(mod(st,1.));
    //

    // Define the centre of current Tile
    vec2 tileCentre = vec2(patternIndex) + vec2(.5);


    // Draw the tile
    vec3 colorMask;
    // Get a Hash from the Pattern Index and map it to [0...3]. This index
    // is then used to determine the Sphere Orientation
    int index = int(LIB_random(vec2(patternIndex)) * 4.);

    if(index == 0) {
        // Keep as is
        colorMask = vec3(1.0, 0.6157, 0.0);
    }
    else if(index == 1) {
        // rotate by 90°
        local_st = rotateLocal2D(local_st, .5*PI);
        colorMask = vec3(0.9216, 0.4078, 0.0667);

    }
    else if(index == 2) {
        // rotate by 180°
        local_st = rotateLocal2D(local_st, PI);
        colorMask = vec3(0.8196, 0.4745, 0.0275);

    }
    else if(index == 3) {
        // rotate by 270°
        local_st = rotateLocal2D(local_st, 1.5*PI);
        colorMask = vec3(1.0, 0.7294, 0.3725);
    }

    // Get Distance between Mouse Cursor and st
    float distanceFromMouseToCurrentTileCentre = distance(mx, tileCentre);
    
    // Set up "supression" from Mouse Cursor
    float fraction = distanceFromMouseToCurrentTileCentre / (float(PatternDensity) * .7);
    if(fraction > 1.) {
        fraction = 1.; // Clamp value so that you dont get "over-filled" Quarter Circles.
    }

    // Debug Case to see if distance is calculated correctly.
    if(!true) {
        gl_FragColor = vec4(vec3(fraction), 1.);
        return;
    }

    // Radius is multiplied with fraction which depends on distance of 
    float eval = evalCircle(1. * fraction, local_st);
    if(eval > .0)
        gl_FragColor = vec4(eval * colorMask, 1.);
    else 
        gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
}