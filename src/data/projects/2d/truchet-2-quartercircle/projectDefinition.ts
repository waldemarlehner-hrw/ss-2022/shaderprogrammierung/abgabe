import Project from "../../../Project";

import frag from "./truchet-2-quartercircle.glsl";

export default new Project(
    "Truchet 2: Quarter Circle", 
    "Implementierung der Quarter Circles aus Creative Coding als Truchet Tiles", 
    "truchet2", 
    (frag),
    "https://gitlab.com/waldemarlehner-hrw/ss-2022/shaderprogrammierung/abgabe/-/blob/master/src/data/projects/2d/truchet-2-quartercircle/truchet-2-quartercircle.glsl"    
);

