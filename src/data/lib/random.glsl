
float LIB_random(vec2 st) {
    // From Book of Shaders
    return fract(sin(dot(st.xy, vec2(12.9898,78.233)))*43758.5453123);
}

float LIB_random(vec3 r) {
    float t = LIB_random(r.xy);
    return LIB_random(vec2(t, r.z));
}



float LIB_random(float s) {
    return LIB_random(vec2(s));
}


#pragma glslify: export(LIB_random)