#pragma glslify: LIB_random = require("./random.glsl") 
float LIB_noise(vec2 value) {
    vec2 i = floor(value);
    vec2 f = fract(value);

    // Four corners in 2D of a tile
    // Sample 2 corners
    float TL = LIB_random(i + vec2(0.0, 0.0));
    float TR = LIB_random(i + vec2(1.0, 0.0));
    float BL = LIB_random(i + vec2(0.0, 1.0));
    float BR = LIB_random(i + vec2(1.0, 1.0));

    // Map linear 0->1 to smoothed smoothstep. This way we get a more "smoothed" 
    // 
    vec2 u = smoothstep(0.,1.,f);

    // weight based on ditance to corners
    return mix(TL, TR, u.x) +
            (BL - TL)* u.y * (1.0 - u.x) +
            (BR - TR) * u.x * u.y;
} 

#pragma glslify: export(LIB_noise)