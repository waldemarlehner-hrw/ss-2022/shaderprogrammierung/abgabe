

// Turns inputs (u_mouse, gl_FragCoord.xy) into a centered, aspect-ratio corrected
// space
vec2 LIB_normalizeInput(in vec2 p) {
    p = p / u_resolution.xy; //eg. [(1920, 1080) -> (1,1)]
    // correct aspect ratio
    if (u_resolution.x > u_resolution.y) {
        p.x *= u_resolution.x / u_resolution.y;
        p.x += (u_resolution.y - u_resolution.x) / u_resolution.y / 2.0;
    } else {
        p.y *= u_resolution.y / u_resolution.x;
        p.y += (u_resolution.x - u_resolution.y) / u_resolution.x / 2.0;
    }
    // centering
    p -= 0.5;
    return p;
}

#pragma glslify: export(LIB_normalizeInput)