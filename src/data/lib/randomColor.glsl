#pragma glslify: LIB_random = require("./random.glsl")

// Generate a Random colour using out random Implementation
vec3 LIB_randomColor(float s) {
    float r = LIB_random(s);
    float g = LIB_random(s+.432);
    float b = LIB_random(s-.131);

    return vec3(r,g,b);
    
}

vec3 LIB_randomColor(vec2 s) {
    float t = LIB_random(s);
    return LIB_randomColor(t);
}

vec3 LIB_randomColor(vec3 s) {
    float t = LIB_random(s.xy);
    return LIB_randomColor(vec2(t,s.z));
}
#pragma glslify: export(LIB_randomColor)
