export default class Project {
    constructor(
        public readonly name: string, 
        public readonly description: string, 
        public readonly id: string, 
        public readonly fragmentShader: string,
        public readonly gitCodeUrl: string,
        public readonly uniforms?: Record<string, any>
        ) {

    }
}