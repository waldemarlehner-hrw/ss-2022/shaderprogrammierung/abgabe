import { createRouter, createWebHashHistory, createWebHistory, RouteRecordRaw } from "vue-router";
import LandingPage from "./pages/LandingPage.vue" 
import ProjectPage from "./pages/ProjectPage.vue" 

const routes: RouteRecordRaw[] = [
    {
        path: "/",
        component: LandingPage,
        name: "Landing"
    },
    {
        path: "/projects/:id",
        component: ProjectPage
    },
    {
        path: "/:pathMatch(.*)",
        redirect: {name: "Landing"}
    }
];

export default createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes
})