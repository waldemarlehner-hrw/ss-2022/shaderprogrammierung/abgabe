import { defineConfig, Plugin, transformWithEsbuild } from 'vite'
import vue from '@vitejs/plugin-vue'
import { quasar, transformAssetUrls } from '@quasar/vite-plugin'
// @ts-ignore
import { execSync } from 'child_process';

function getBaseUrl() {
  const isCI = process.env.GITLAB_BUILD == "1";
  if(!isCI) {
    return "";
  }
  const url = new URL(process.env.CI_PAGES_URL);
  return url.pathname+url.search;
}



// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue({template: {transformAssetUrls}}), 
    quasar({
      sassVariables: "src/quasar-variables.sass",
      autoImportComponentCase: 'pascal',
    }),
    basicGlslifyPlugin()
  ],
  base: getBaseUrl()
})


const fileRegex = /\.(glsl)$/
function basicGlslifyPlugin() : Plugin {
    return {
        name: "glslify-basic",

        async transform(src: string, id: any) {
            if(fileRegex.test(id)) {
                const result = execSync("glslify "+id).toString();
                return await transformWithEsbuild(
                  result,
                  id, {
                    loader: "text",
                    format: "esm"
                  }
                )
            }
        }
    }
}